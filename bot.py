import requests
from model import PasswordModel

class TG:

	def __init__(self, token):
		self._base_url = "https://api.telegram.org/bot" + token

	def getMe(self):
		url = self._base_url + '/getMe'
		data = requests.get(url = url).json()
		if data['ok']: return data['result']

	def sendMessage(self, chat_id, text):
		url = self._base_url + '/sendMessage'
		params = {'chat_id':chat_id,'text':text}
		data = requests.get(url=url, params=params).json()
		if data['ok']: return data['result']

	def getUpdates(self, offset, limit):
		url = self._base_url + "/getUpdates"
		params = {'offset':offset,'limit':limit}
		data = requests.get(url=url, params=params).json()
		return data['result']

def read_config(path):
	with open(path, 'r') as f:
		return {k.strip(): v.strip() for i in \
			[l for l in f.readlines() if l.strip() != ''] for k,v in [i.split('=')]}

def load_offset(path):
	with open(path, 'r') as f:
		return int(f.read())

def dump_offset(path, offset):
	with open(path, 'w') as f:
		f.write(str(offset))

def main():
	
	c = read_config('data/bot.conf')
	offset = load_offset('data/bot.offset')
	
	tg = TG(c['tg_token'])

	model = PasswordModel(
		'data/passwords_1m.txt', 
		'data/names_22k.txt', 
		'data/words_30k.txt', 
		'data/model.cbm')

	while True:

		for update in tg.getUpdates(offset, c['tg_limit']):

			if 'message' in update:
				
				# print(f'offset: {offset}')
				message = update['message']
				update_id = update['update_id']
				user_id = message['from']['id']
				offset = update_id + 1

				if 'text' in message:
					passw = message['text']
					passw_freq = model.predict(passw)
					tg.sendMessage(user_id, passw_freq)

			dump_offset('data/bot.offset', offset)


if __name__ == '__main__': main()
