from model import PasswordModel
from flask import Flask, request, render_template

model = PasswordModel(
    'data/passwords_1m.txt', 
    'data/names_22k.txt', 
    'data/words_30k.txt', 
    'data/model.cbm')

app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        pw = request.form['password']
        if not pw: pw = 'qwerty'
        pass_freq = model.predict(pw)
        return render_template('index.html', password=pw, prediction=pass_freq)
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', threaded=False, debug=True)