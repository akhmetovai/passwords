from math import exp
from catboost import CatBoostRegressor
from password_strength import PasswordStats

class TopPasswords:

    def __init__(self, path):
        i = 0
        f = open(path)
        self.d = dict()
        for l in f:
            i = i + 1
            self.d[l[:-1]] = i
        f.close()

    def get_idx(self, passw):
        return self.d[passw] if passw in self.d else 0


class TopNames:

    def __init__(self, path):
        i = 0
        f = open(path)
        self.d = dict()
        for l in f:
            i = i + 1
            n = l[:-1].lower()
            self.d[n] = i
        f.close()

    def get_idx(self, name):
        return self.d[name.lower()] if name.lower() in self.d else 0


class TopWords:

    def __init__(self, path):
        i = 0
        f = open(path)
        self.d = dict()
        for l in f:
            i = i + 1
            n = l[:-2].lower()
            self.d[n] = i
        f.close()

    def get_idx(self, word):
        return self.d[word.lower()] if word.lower() in self.d else 0

class PasswordModel:

    def __init__(self, passw_path, names_path, words_path, model_path):
        self.top_passw = TopPasswords(passw_path)
        self.top_names = TopNames(names_path)
        self.top_words = TopWords(words_path)
        self.model = CatBoostRegressor()
        self.model.load_model(model_path)

    def _get_features(self, passw):
        
        stats = PasswordStats(passw)
        top_pass_idx = self.top_passw.get_idx(passw)
        top_name_idx = self.top_names.get_idx(passw)
        top_word_idx = self.top_words.get_idx(passw)
        
        x = [len(passw),
            top_pass_idx,
            int(top_pass_idx == 0),
            top_name_idx,
            int(top_name_idx == 0),
            top_word_idx,
            int(top_word_idx == 0),
            stats.strength(),
            stats.letters,
            stats.entropy_bits,
            stats.entropy_density if len(passw) > 1 else 0,
            stats.letters_lowercase,
            stats.letters_uppercase,
            stats.numbers,
            stats.repeated_patterns_length,
            stats.sequences_length,
            stats.special_characters]
        
        return x

    def predict(self, passw):
        x = self._get_features(passw)
        return exp(self.model.predict([x])[0]) - 1


def main():
    model = PasswordModel(
        'data/passwords_1m.txt', 
        'data/names_22k.txt', 
        'data/words_30k.txt', 
        'data/model.cbm')
    print(model.predict('qwerty'))


if __name__ == '__main__': main()
